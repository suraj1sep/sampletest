import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_emp_info/emp_info/emp_details.dart';

import '../main.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var res, emps;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Employee Info"),
      ),
      body: new Container(
        child: new Center(
          child: new FutureBuilder(
            future: DefaultAssetBundle
                .of(context)
                .loadString('assets/json/employees.json'),
            builder: (context, snapshot) {
              var empdata = json.decode(snapshot.data.toString());
              return new ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => EmpDetail(emp: empdata[index]),
                      ),
                    ),
                    child: new Card(
                      margin: new EdgeInsets.all(2.0),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: new Row(
                          children: <Widget>[
                            new Image.network(empdata[index]['imageUrl'],height: 100.0,),
                            new Text("Name: " + empdata[index]['firstName'] + " " + empdata[index]['lastName'], style: new TextStyle(fontSize: 15.0)),
                          ],
                        ),
                      ),
                    /*  onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => EmpDetail(emp: empdata),
                          ),
                        );
                        }*/
                    ),
                  );
                },
                itemCount: empdata == null ? 0 : empdata.length,
              );
            },
          ),
        ),
      ),
    );
  }
}