import 'dart:convert';
import 'package:flutter/material.dart';

import '../main.dart';

class EmpDetail extends StatelessWidget {
  final emp;

  const EmpDetail({Key key, @required this.emp}) : super(key: key);
  @override
  Widget build(BuildContext context) {
      return new Scaffold(
      appBar: new AppBar(
      title: new Text("Employee Info Details"),
    ),
        body: new Container(
          child: new Center(
            child: new FutureBuilder(
              future: DefaultAssetBundle
                  .of(context)
                  .loadString('assets/json/employees.json'),
              builder: (context, snapshot) {
                var empdata = json.decode(snapshot.data.toString());
                return new ListView.builder(
                  itemBuilder: (BuildContext context, int index) {
                    return new Card(
                      margin: new EdgeInsets.all(2.0),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Image.network(empdata[index]['imageUrl'],height: 100.0,),
                            new Text("Name: " + empdata[index]['firstName'] + " " + empdata[index]['lastName'], style: new TextStyle(fontSize: 15.0)),
                            new Text("Email: " + empdata[index]['email'], style: new TextStyle(fontSize: 12.0)),
                            new Text("Contact Number: " + empdata[index]['contactNumber'] +  " " +  " " + "DOB :" + " " + empdata[index]['dob'], style: new TextStyle(fontSize: 12.0)),
                            new Text("Address: " + empdata[index]['address'], style: new TextStyle(fontSize: 12.0)),
                          ],
                        ),
                      ),
                    );
                  },
                  itemCount: empdata == null ? 0 : empdata.length,
                );
              },
            ),
          ),
        ),
    );
  }
}
