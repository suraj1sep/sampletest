import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_emp_info/emp_info/homepage.dart';

void main() => runApp(new MaterialApp(
  theme: new ThemeData(
    primarySwatch: Colors.teal,
  ),
  home: new HomePage(),
));
